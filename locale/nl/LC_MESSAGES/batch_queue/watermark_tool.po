# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-02 00:35+0000\n"
"PO-Revision-Date: 2023-06-22 10:11+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: ../../batch_queue/watermark_tool.rst:1
msgid "digiKam Batch Queue Manager Base Tools"
msgstr "Basishulpmiddelen voor Takenwachtrijbeheerder van digiKam"

#: ../../batch_queue/watermark_tool.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, batch, queue, manager, watermark, image, text"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, bulk, wachtrij, beheerder, watermerk, afbeelding, tekst"

#: ../../batch_queue/watermark_tool.rst:14
msgid "Watermark"
msgstr "Watermerk"

#: ../../batch_queue/watermark_tool.rst:16
msgid ""
"While there are many ways to protect your photos from unauthorized use, "
"watermarking still remains the simplest and probably the most effective "
"technique that can help you to identify you as the creator and make it "
"difficult to use your works without permission."
msgstr ""
"Terwijl er vele manieren zijn om uw foto's te beveiligen tegen "
"ongeautoriseerd gebruik, blijft een watermerk aanbrengen nog steeds de "
"eenvoudigste en waarschijnlijk de meest effectieve techniek die u kan helpen "
"om u als de maker te identificeren en het moeilijk maken om uw werken zonder "
"toestemming te gebruiken."

#: ../../batch_queue/watermark_tool.rst:18
msgid ""
"digiKam supports watermarking. The watermarking function in digiKam is "
"available under the Batch Queue Manager tool which you can use to watermark "
"multiple photos in one go. Drag the photos you want to watermark from a "
"digiKam album onto the **Queues** pane to add them to the current queue. "
"Click on the **Base Tools** tab in the **Control Panel** pane and double-"
"click on the **Decorate/Add Watermark** tool to add it to the **Assigned "
"Tools** pane."
msgstr ""
"digiKam ondersteunt het aanbrengen van een watermerk, de functie van "
"aanbrengen van een watermerk digiKam is beschikbaar onder het hulpmiddel "
"Takenwachtrijbeheerder die u kunt gebruiken om in meerdere foto's een "
"watermerk aan te brengen in een gang. Sleep de foto's waarin u een watermark "
"wilt aanbrengen uit een digiKam-album in het paneel **Wachtrijen** om ze aan "
"de huidige wachtrij toe te voegen. Klik op het tabblad **Basishulpmiddelen** "
"in het paneel **Besturingspaneel** en dubbelklik op het hulpmiddel "
"**Versieren/Watermark toevoegen** om het aan het paneel **Toegekende "
"hulpmiddelen** toe te voegen."

#: ../../batch_queue/watermark_tool.rst:20
msgid "Contents"
msgstr "Inhoud"

#: ../../batch_queue/watermark_tool.rst:23
msgid "Lead Settings"
msgstr "Leidende instellingen"

#: ../../batch_queue/watermark_tool.rst:25
msgid ""
"digiKam can use **Text** or **Image** as watermarks, and you can choose the "
"desired watermark type at the top of the Tool Settings pane with the "
"**Watermark type** option. Check **Use Absolute Size** option if you want "
"the watermark to use the given size of the font or the image without any "
"adjustment to the actual image."
msgstr ""
"digiKam kan **Tekst** of **Afbeelding** als watermerk aanbrengen en u kunt "
"het gewenste type watermerk kiezen bovenaan het paneel "
"Hulpmiddelinstellingen met de optie **Type watermerk**. Activeer de optie "
"**Absolute grootte gebruiken** als u het te gebruiken watermark de gegeven "
"grootte van het lettertype of de afbeelding wilt geven zonder enige "
"aanpassing aan de actuele afbeelding."

#: ../../batch_queue/watermark_tool.rst:31
msgid "The Batch Queue Manager Watermark Tool Lead Settings"
msgstr ""
"Het hulpmiddel Leidende instellingen voor het watermerk in de "
"takenwachtrijbeheerder"

#: ../../batch_queue/watermark_tool.rst:34
msgid "Image Settings"
msgstr "Afbeeldingsinstellingen"

#: ../../batch_queue/watermark_tool.rst:36
msgid ""
"If you want to use the **Image** watermark type, make sure that you already "
"have a graphics file handy, and select it from the file system. Check "
"**Ignore Watermark aspect Ratio** option if you want the watermark to ignore "
"its own aspect ratio and use the image's aspect ratio instead. Check **Add "
"transparency to watermark image** if you want watermark to be transparent "
"and tune the **Opacity** value in percentages with the option just below."
msgstr ""
"Als u het type watermerk **Afbeelding** wilt gebruiken, dan moet u al een "
"grafisch bestand bij de hand hebben en het selecteren uit het "
"bestandssysteem. Activeer de optie **Beeldverhouding van watermerk negeren** "
"als u wilt dat het watermerk zijn eigen beeldverhouding moet negeren en de "
"beeldverhouding van de afbeelding in plaats daarvan moet gebruiken. Activeer "
"**Transparantie aan watermerkafbeelding toevoegen** als u het watermerk "
"transparant wilt hebben en regel de waarde van **Dekking** in percentages af "
"met de optie er juist onder."

#: ../../batch_queue/watermark_tool.rst:43
msgid "The Batch Queue Manager Watermark Tool Image Settings"
msgstr ""
"Het hulpmiddel watermerk in de afbeelding instellen in de "
"takenwachtrijbeheerder"

#: ../../batch_queue/watermark_tool.rst:46
msgid "Text Settings"
msgstr "Tekstinstellingen"

#: ../../batch_queue/watermark_tool.rst:48
msgid ""
"The **Text** watermark is more simple to use as it does not require an "
"external source of contents to generate the mark over the images. In "
"**Watermark text** field, enter the desired string to use superimposed "
"contents. Below, you can tune the **Font** properties to write text on "
"images, as the **name**, the **style**, and the **color** (the **size** is "
"auto-calculated). With **Text opacity** you can adjust the transparency of "
"the watermark text, where 100 is fully opaque and 0 is fully transparent. "
"Finally, the **Use background** allows to draw the text over a colored "
"background where you can choose the color and the transparency level."
msgstr ""
"Het watermerk **Tekst** is eenvoudiger te gebruiken omdat het geen externe "
"bron of inhoud nodig heeft om het merk over de afbeeldingen aan te brengen. "
"In het veld **Watermerktekst**, voer de gewenste tekenreeks in om de inhoud "
"te overlappen. Onderstaand kunt u de eigenschappen van **Lettertype** van de "
"te schrijven tekst op afbeeldingen afregelen, zoals the **naam*, de "
"**stijl** en de **kleur** (de **grootte** wordt automatisch berekend). Met "
"**Tekstdekking** kunt u de transparantie van de watermerktekst aanpassen, "
"waar 100 volledige dekking is en 0 is volledig transparant. Tenslotte, biedt "
"**Achtergrond gebruiken** om de tekst over een gekleurde achtergrond te "
"tekenen waar u de kleur en het transparantieniveau kunt kiezen."

#: ../../batch_queue/watermark_tool.rst:54
msgid "The Batch Queue Manager Watermark Tool Text Settings"
msgstr ""
"Het hulpmiddel Tekstinstellingen voor het watermerk in de "
"takenwachtrijbeheerder"

#: ../../batch_queue/watermark_tool.rst:58
msgid "Geometry Settings"
msgstr "Geometrie-instellingen"

#: ../../batch_queue/watermark_tool.rst:60
msgid ""
"Specify in this view the watermark geometry settings such as **Placement "
"Position**, **Rotation**, **Size**, and **Margins**."
msgstr ""
"Specificeren in deze weergave de geometrie-instellingen van het watermark "
"zoals **Plaatsing van positie**, **Rotatie**, **Grootte** en **Marges**."

#: ../../batch_queue/watermark_tool.rst:62
msgid ""
"**Placement Type** is the lead geometry setting which will enable or disable "
"other options from this view. **Specific Location** allows to set a static "
"position of the watermark. **Systematic Repetition** will place the "
"watermark in loop to cover the images. **Random Repetition** will place the "
"watermark randomly over the images."
msgstr ""
"**Type plaatsing** is de leidende geometrie-instelling die andere opties van "
"deze weergave zal in of uitschakelen. **Specifieke locatie** biedt het "
"zetten van een statische positie van het watermerk. **Systematische "
"herhaling** zal het watermerk in een lus de afbeeldingen bedekken. "
"**Willekeurige herhaling** zal het watermerk willekeurig over de "
"afbeeldingen plaatsen."

#: ../../batch_queue/watermark_tool.rst:64
msgid ""
"**Density of watermark repetition** is Disabled in **Specific Location** "
"mode. When you choose to have the watermark repeated many times in the "
"**Placement Type**, you can specify here whether the repetition."
msgstr ""
"**Dichtheid van herhaling van watermerk** is uitgeschakeld in modus "
"**Specifieke locatie**. Wanneer u er voor kiest om het watermerk vele malen "
"te herhalen in het **Type plaatsing**, dan kunt u hier specificeren of de "
"herhaling."

#: ../../batch_queue/watermark_tool.rst:66
msgid ""
"**Randomize watermark orientation** is enabled in **Random Repetition** mode "
"only. When you choose to have the watermark repeated randomly in the "
"**Placement Type**, you can specify here to randomly rotate the watermark "
"(0, 90, 180, 270 degrees)."
msgstr ""
"**Willekeurige oriëntatie van watermerk** is alleen ingeschakeld in modus "
"**Willekeurige herhaling**. Wanneer u er voor kiest om het watermerk vele "
"malen willekeurig te herhalen in **Type plaatsing**, dan kunt u hier "
"specificeren om het watermerk willekeurig (0, 90, 180, 270 graden) te "
"draaien."

#: ../../batch_queue/watermark_tool.rst:68
msgid ""
"Use **Sparsity Factor** option to get more control over the sparsity of "
"watermark repetition. The higher the value the sparser the watermarks get. "
"Use floating point values, typically between 1.0 and 3.0. It can also be "
"less than 1.0."
msgstr ""
"Deze optie **Spreidingsfactor** gebruiken om meer controle te krijgen over "
"de spreiding van herhaling van het watermerk. Hoe hoger de waarde hoe meer "
"spreiding het watermerk krijgt. Gebruik drijvende-komma waarden, typisch "
"tussen 1,0 en 3,0. Het kan ook kleiner zijn dan 1,0."

#: ../../batch_queue/watermark_tool.rst:70
msgid ""
"**Placement Position** allows to specify the area on the image to write the "
"watermark. Available values are **Top left**, **Top right**, **Bottom "
"left**, **Bottom right**, **Center**, **Top center**, and **Bottom center**."
msgstr ""
"**Plaatsingspositie** bidt het specificeren van het gebied op de afbeelding "
"waar het watermerk te schrijven. Beschikbare waarden zijn **Linksboven**, "
"**Rechtsboven**, **Linksonder**, **Rechtsonder**, **Centrum**, **Midden "
"boven** en **Midden onder**."

#: ../../batch_queue/watermark_tool.rst:72
msgid ""
"**Rotation** allows to specify the amount of degrees to rotate the "
"watermark. Available values are **0 degrees**, **90 degrees CW**, **180 "
"degrees**, and **270 degrees CW**."
msgstr ""
"**Rotatie** biedt het specificeren van het aantal te draaien graden van het "
"watermerk. Beschikbare waarden zijn **0 graden**, **90 graden rechtsom**, "
"**180 graden** en **270 graden rechtsom**."

#: ../../batch_queue/watermark_tool.rst:74
msgid ""
"**Size** setting allows to specify the size of watermark, as a percentage of "
"the marked image."
msgstr ""
"**Grootte** instelling biedt het specificeren van de grootte van het "
"watermerk, als een percentage van de gemerkte afbeelding."

#: ../../batch_queue/watermark_tool.rst:76
msgid ""
"**X margin** and **Y margin** allow to specify the margin from the edge in X "
"and Y directions, as a percentage of the marked image."
msgstr ""
"**X-marge** en **Y-marge** biedt het specificeren van de marge vanaf de rand "
"in X en Y richting, als een percentage van de gemerkte afbeelding."

#: ../../batch_queue/watermark_tool.rst:82
msgid "The Batch Queue Manager Watermark Tool Geometry Settings"
msgstr ""
"Het hulpmiddel Geometrie-instellingen voor het watermerk in de "
"takenwachtrijbeheerder"

#: ../../batch_queue/watermark_tool.rst:85
msgid "Results"
msgstr "Resultaten"

#: ../../batch_queue/watermark_tool.rst:87
msgid ""
"Once you are satisfied with the settings, hit the **Run** button, and once "
"digiKam’s finished, you’ll find the watermarked photos in the target folder. "
"See below the samples of text and image watermarks applied to the same image "
"using transparency."
msgstr ""
"Nadat u tevreden bent met de instellingen, druk de knop **Uitvoeren** in en "
"nadat digiKam is geëindigd, zult u de foto's voorzien van een watermerk in "
"de doelmap vinden. Zie onderstaande voorbeelden van een tekst- en "
"afbeeldingswatermerk toegepast op dezelfde afbeelding met gebruik van "
"transparantie."

#: ../../batch_queue/watermark_tool.rst:94
msgid ""
"Sample of Centered Image Watermark Applied to a photo Using Transparency"
msgstr ""
"Voorbeeld van een gecentreerd afbeeldingswatermerk toegepast op een foto met "
"gebruik van transparantie"

#: ../../batch_queue/watermark_tool.rst:101
msgid ""
"Sample of Systematic Repeated Text Watermark Applied to a photo Using "
"Transparency"
msgstr ""
"Voorbeeld van een systematisch herhaald tekstwatermerk toegepast op een foto "
"met gebruik van transparantie"
