# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-02 00:35+0000\n"
"PO-Revision-Date: 2023-03-17 08:49+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../main_window/timeline_view.rst:1
msgid "digiKam Main Window Timeline View"
msgstr "Панель розкладу основного вікна digiKam"

#: ../../main_window/timeline_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, timeline, days, weeks, months, years"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, навчання, простий, розклад, дні, тижні, місяці, роки"

#: ../../main_window/timeline_view.rst:14
msgid "Time-Line View"
msgstr "Панель перегляду розкладу"

#: ../../main_window/timeline_view.rst:20
msgid "The Timeline View"
msgstr "Панель перегляду розкладу"

#: ../../main_window/timeline_view.rst:22
msgid ""
"The Timeline View shows a timescale-adjustable histogram of the numbers of "
"images per **Time Unit** which is selectable by a drop down field. Available "
"graininess of time are **Day**, **Week**, **Month**, and **Year**."
msgstr ""
"У режимі перегляду шкали часу програма покаже вам придатну до налаштовування "
"гістограму із розподілом зображень за часом. Одиницю часу можна вибрати зі "
"спадного списку. Доступними одиницями є **день**, **тиждень**, **місяць** і "
"**рік**."

#: ../../main_window/timeline_view.rst:24
msgid ""
"To the right of that you can choose between a **Linear** or **Logarithmic** "
"histogram using scaling buttons. A selection frame can be moved over the "
"histogram and to display the photographs out of a certain time frame, just "
"click on the corresponding histogram bar. You are not restricted to one bar: "
"with :kbd:`Shift+left` click you can select a range of bars from the "
"histogram, and with :kbd:`Ctrl+left` click you can select more single bars "
"to the first one."
msgstr ""
"Праворуч за допомогою кнопок масштабування ви можете вибрати варіант "
"гістограми: лінійна чи логарифмічна. Діапазон позначення можна рухати "
"гістограмою. Щоб побачити усі фотографії у певному часовому діапазоні, "
"просто наведіть вказівник миші на відповідний стовпчик гістограми і клацніть "
"лівою кнопкою миші. Можна позначити і декілька стовпчиків. Додати стовпчики "
"до першого можна за допомогою комбінації :kbd:`Shift` і клацання лівою "
"кнопкою миші для позначення діапазону стовпчиків або :kbd:`Ctrl` і клацання "
"лівою кнопкою миші для позначення додаткових стовпчиків на додачу до першого."

#: ../../main_window/timeline_view.rst:31
msgid "Timeline Selection Screencast"
msgstr "Вибір на панелі розкладу"

#: ../../main_window/timeline_view.rst:33
msgid ""
"In the field right below you can enter a title and save your selection. It "
"will then appear in the **Searches** list field below. But the best is still "
"to come: the Timeline View offers a search for a search. If you have a lot "
"more searches saved in the database, the adaptive search field at the bottom "
"may help to find a certain entry in the list."
msgstr ""
"За допомогою поля, розташованого нижче, ви можете вказати назву і зберегти "
"позначене. Цю назву буде згодом показано у списку «Результати пошуку». Але "
"це ще не все: у режимі перегляду шкали часу ви можете шукати пошук! Якщо у "
"вас збережено багато результатів пошуку, ви можете скористатися полем для "
"пошуку, розташованим внизу, для того, щоб знайти потрібний вам запис у "
"списку."

#: ../../main_window/timeline_view.rst:35
msgid ""
"The date-range of the histogram is populated with the time stamp of items "
"registered in the database. For each date matching the histogram bars time-"
"resolution, an item is counted in the statistics. Long bars correspond to "
"dates where a lots of items have been taken in the same graininess-range."
msgstr ""
"Діапазон дат для гістограми буде заповнено на основі часової позначки "
"записів, яку зареєстровано у базі даних. Для кожної дати, що відповідає "
"роздільності стовпчиків гістограми, буде обчислено статистичні дані. Довгі "
"стовпчики відповідатимуть датам із великою кількістю записів, що потрапляють "
"у той самий діапазон часу."

#: ../../main_window/timeline_view.rst:37
msgid ""
"The icon-view gives the search results of the selection of date. You can "
"select wanted items to post-process contents on batch queue manager or "
"export items to the Internet. From Right sidebar you can filter icon view "
"contents by database properties."
msgstr ""
"На панелі перегляду піктограм буде показано результати для позначеної дати. "
"Ви можете позначити бажані записи для остаточної обробки вмісту або "
"експортування до інтернету. За допомогою правої бічної панелі ви можете "
"фільтрувати вміст панелі перегляду піктограм за властивостями з бази даних."
